setlocal ts=2       " Tab stop of 2
setlocal sw=2       " Indent of 2
setlocal tw=72      " Wrap at 72 characters
